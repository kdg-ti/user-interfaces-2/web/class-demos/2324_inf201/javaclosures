package be.kdg.ui2;
import java.util.function.*;

public class CountingCrows {

	public static void main(String[] args) {
		String message = "crows";
		IntConsumer showMessage =  nr -> System.out.println(nr +" " + message);
		showMessage.accept(3);
		IntSupplier counter = makeCounter();
		showMessage.accept(counter.getAsInt());
		showMessage.accept(counter.getAsInt());
		IntSupplier otherCounter = makeCounter();
		showMessage.accept(otherCounter.getAsInt());


	}

	public static IntSupplier makeCounter() {
		int start = 2;
		return () -> start;
		// variable used in lambda should be final or effectively final!
		//return () -> start++;
	}
}
