package be.kdg.ui2;
import java.util.function.DoubleFunction;

public class Multi {
	public static void main(String[] args) {
		DoubleFunction<Double> fiveFold = getMultiplier(5);
		DoubleFunction<Double> twice = getMultiplier(2);
		System.out.println(fiveFold.apply(3.0));
		System.out.println(twice.apply(3.0));
	}

	public static DoubleFunction<Double> getMultiplier(double factor) {
		return input -> input * factor;
	}
}
