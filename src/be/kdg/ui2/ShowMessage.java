package be.kdg.ui2;

import java.util.function.IntConsumer;

public class ShowMessage {

	public static void main(String[] args) {
		String message = "crows";
		IntConsumer showMessage =  nr -> System.out.println(nr + message);
		showMessage.accept(3);
		// variable used in lambda should be final or effectively final!
		//message ="doves";
		showMessage.accept(4);
	}
}
